// Import the Task model from the models folder by using the 'require' directive
const Task = require('../models/Task.js')

// Create a new task in MongoDB Collection
module.exports.createTask = (requestBody) => {
	let newTask = new Task ({
		name: requestBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			return error
		}

		return 'Task created successfully!'
	})
}


// Updates an existing task inside MongoDB Collection
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then ((result, error) => {
		if(error){
			return error
		}

		result.status = newContent.status

		return result.save().then((updatedTask, error) =>{
			if(error){
				return error
			}

			return updatedTask
		})
	})
}


module.exports.getOneTask = (getTaskId) => {
	return Task.findById(getTaskId).then((oneTask, error) => {
		if(error){
			return error
		}
		return oneTask
	})
}
