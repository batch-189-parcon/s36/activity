const express = require('express')
const TaskController = require ('../controllers/TaskController.js')
const router = express.Router()


router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((task) => response.send(task))
})



router.put('/:id/complete', (request, response) => {

	TaskController.updateTask(request.params.id,request.body).then((updatedTask) => response.send(updatedTask))
})



//Route for /api/task/id/find - Runs the getTask function from the controller
router.get('/:id/find', (request, response) => {
	TaskController.getOneTask(request.params.id).then((oneTask) => response.send(oneTask))
})




module.exports = router